{ config, pkgs, ... }:

{
  imports =
    [ # include the results of the hardware scan.
      ./hardware-configuration.nix
    ];
    
  boot.loader.grub = 
  {
    enable = true;
    version = 2;
  };
  boot.loader.grub.device = "/dev/sda";
  
  boot.cleanTmpDir = true;
  
  networking.hostName = "brownix";
  
  time.timeZone = "Europe/Amsterdam";
  
  environment.systemPackager = with pkgs; [
      wget vim emacs neovim git
      ];
      
  services.openssh.enable = true;
  services.virtualbox.enable = true;
  
  users.users.pvinis = {
      isNormalUser = true; 
      extraGroups = [ "wheel" ];
  };
  
  system.stateVersion = "18.09";
  
  
}